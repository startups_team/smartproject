<?php

namespace App\Http\Controllers;

use App\Models\reports\rolling_retention\RollingRetention;
use App\Models\reports\rolling_retention\SolitaireRollingRetention;
use App\Models\reports\rolling_retention\SolitaireRollingRetentionCache;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    const START_DATE = '2021-01-01';
    const END_DATE = '2021-01-31';
    const DATE_SEPARATOR = ' - ';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $startScriptTime = microtime(true);

        $startDate = self::START_DATE;
        $endDate = self::END_DATE;
        $data = [];
        $useCache = true;

        if ($dates = $request->get('daterange')) {
            $dates = $this->getDates($dates);
            $startDate = $dates['startDate'];
            $endDate = $dates['endDate'];
            $useCache = $request->get('cache') ? true : false;

            $cache = new SolitaireRollingRetentionCache();
            $report = new SolitaireRollingRetention(
                $startDate,
                $endDate,
                RollingRetention::DEFAULT_WINDOW_DAYS
            );
            if ($useCache) {
                $report->setCache($cache);
            }
            $report->startReport();
            $data = $report->getReportWithFooter();
        }

        $endScriptTime = microtime(true);
        $scriptTime = $endScriptTime - $startScriptTime;
        return view('project.index',
            [
                'time' => $scriptTime,
                'data' => $data,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'useCache' => $useCache
            ]
        );
    }

    private function getDates($dates)
    {
        $dateArray = explode(self::DATE_SEPARATOR, $dates);

        return [
            'startDate' => date('Y-m-d', strtotime($dateArray[0])),
            'endDate' => date('Y-m-d', strtotime($dateArray[1]))
        ];
    }
}
