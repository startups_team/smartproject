<?php


namespace App\Models\reports;


class DateReport
{
    protected $startDate;
    protected $endDate;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /*
     * @return array
     */
    public function getAllDates()
    {
        return $this->getDatePeriod($this->startDate, $this->endDate);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return array
     * @throws \Exception
     */
    protected function getDatePeriod($startDate, $endDate)
    {
        $result = [];
        $start = new \DateTime($startDate);
        $interval = new \DateInterval('P1D');
        $end = new \DateTime($endDate);

        $period = new \DatePeriod($start, $interval, $end);
        foreach ($period as $date) {
            $result[] = $date->format('Y-m-d');
        }
        $result[] = $endDate;

        return $result;
    }

}
