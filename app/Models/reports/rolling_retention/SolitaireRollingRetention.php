<?php


namespace App\Models\reports\rolling_retention;


class SolitaireRollingRetention extends RollingRetention
{
    const TABLE_NAME = 'user_visits';
    const DATE_FIELD = 'date';
    const USER_ID_FIELD = 'user_id';

}
