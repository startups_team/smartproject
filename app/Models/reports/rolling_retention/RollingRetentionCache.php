<?php

namespace App\Models\reports\rolling_retention;

use Illuminate\Database\Eloquent\Model;

class RollingRetentionCache extends Model implements IReportDateCache
{
    /**
     * @param IReportDate $report
     * @param array $dates
     */
    public function setData(IReportDate $report, $dates = [])
    {
        $data = $report->getReportData();

        if ($dates) {
            $data = $this->getDataForDates($data, $dates);
        }

        $windowDays = $report->getWindowDays();

        foreach ($data as $date => $value) {
            $model = static::where('report_date', $date)->where('window_days', $windowDays)->first();

            if (!$model) {
                $model = new static();
                $model->report_date = $date;
                $model->window_days = $windowDays;
            }
            $model->data = $value;
            $model->save();
        }
    }

    /**
     * @param array $data
     * @param array $dates
     * @return array
     */
    protected function getDataForDates(array $data,array $dates)
    {
        $result = [];

        foreach ($dates as $date) {
            if (isset($data[$date])) {
                $result[$date] = $data[$date];
            }
        }

        return $result;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @param int $windowDays
     */
    public function getData($startDate, $endDate, $windowDays)
    {
        $result = [];
        $data = static::where('report_date', '>=' , $startDate)
            ->where('report_date', '<=' , $endDate)
            ->where('window_days', $windowDays)
            ->orderBy('report_date')
            ->get();

        foreach ($data as $row) {
            $result[$row->report_date] = $row->data;
        }

        return $result;
    }
}
