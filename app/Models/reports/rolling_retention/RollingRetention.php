<?php

namespace App\Models\reports\rolling_retention;

use App\Models\reports\DateReport;
use Illuminate\Support\Facades\DB;
use RichJenks\Stats\Stats;

/**
 * Class RollingRetention
 * @package App\Models\reports\rolling_retention
 *
 * EXAMPLE
 * SELECT
 *      countIf(r1 = [1, 0, 1]) 2021_01_15,
 *      countIf(r2 = [1, 0, 1]) 2021_01_16,
 *      countIf(r3 = [1, 0, 1]) 2021_01_17,
 *      countIf(r4 = [1, 0, 1]) 2021_01_18,
 *      countIf(r5 = [1, 0, 1]) 2021_01_19
 * FROM
 *      (
 *          SELECT
 *              user_id,
 *              retention(date = '2021-01-15',date BETWEEN '2021-01-02' AND '2021-01-14',date < '2021-01-02') as r1,
 *              retention(date = '2021-01-16',date BETWEEN '2021-01-03' AND '2021-01-15',date < '2021-01-03') as r2,
 *              retention(date = '2021-01-17',date BETWEEN '2021-01-04' AND '2021-01-16',date < '2021-01-04') as r3,
 *              retention(date = '2021-01-18',date BETWEEN '2021-01-05' AND '2021-01-17',date < '2021-01-05') as r4,
 *              retention(date = '2021-01-19',date BETWEEN '2021-01-06' AND '2021-01-18',date < '2021-01-06') as r5
 *          FROM user_visits
 *          GROUP BY user_id
 *      )
 *
 */
class RollingRetention extends DateReport implements IReportDate
{
    const DEFAULT_WINDOW_DAYS = 14;
    const CHUNK_DATES = 31;
    const DATE_DELIMITER = '_';
    const SELECT_LINE = 'countIf(r{*key*} = [1, 0, 1]) {*date*}';
    const SELECT_LINE_SUBQUERY_START = 'retention(';
    const SELECT_LINE_SUBQUERY_PART1 = '{*date_field*} = \'{*retention_date*}\',';
    const SELECT_LINE_SUBQUERY_PART2 = '{*date_field*} BETWEEN \'{*window_date_start*}\' AND \'{*window_date_end*}\',';
    const SELECT_LINE_SUBQUERY_PART3 = '{*date_field*} < \'{*window_date_start*}\'';
    const SELECT_LINE_SUBQUERY_END = ') as r{*key*}';

    const AVG = 'AVG';
    const MEDIAN = 'MEDIAN';

    protected $dates;
    protected $windowDays;
    protected $reportData = [];
    protected $reportWithFooter = [];
    protected $cache;

    public function __construct($startDate, $endDate, int $windowDays = self::DEFAULT_WINDOW_DAYS)
    {
        parent::__construct($startDate, $endDate);
        $this->windowDays = $windowDays;
    }

    public function startReport()
    {
        if ($this->cache) {
            $this->setDataFromCache();
        } else {
            foreach (array_chunk($this->getAllDates(), self::CHUNK_DATES) as $this->dates) {
                $this->countForDates();
            }
        }
    }

    private function countForDates()
    {
        $sql = $this->getSql();

        $db = DB::connection('clickhouse')->getClient();
        $statement = $db->select($sql);
        $resultQuery = $statement->rows();

        $data = $this->resultQueryToArray($resultQuery);

        $this->reportData = array_merge($this->reportData, $data);
    }

    /**
     * @return array
     */
    public function getReportWithFooter()
    {
        $reportData = $this->getReportData();
        $report = $this->addAvg($reportData, $reportData);

        return $this->addMedian($reportData, $report);
    }

    /**
     * @param array $data
     * @param array $report
     * @return array
     */
    protected function addAvg(array $data, array $report)
    {
        if ($data) {
            $report[self::AVG] = round(Stats::average($data), 2);
        }

        return $report;
    }

    /**
     * @param array $data
     * @param array $report
     * @return array
     */
    protected function addMedian(array $data, array $report)
    {
        if ($data) {
            $report[self::MEDIAN] = Stats::median($data);
        }

        return $report;
    }

    /**
     * @return array
     */
    public function getReportData()
    {
        return $this->reportData;
    }

    /**
     * @return int
     */
    public function getWindowDays()
    {
        return $this->windowDays;
    }

    /**
     * @return string
     */
    private function getSql()
    {
        $result = 'SELECT ';
        $result .= $this->getSelectLines();
        $result .= ' FROM (' . $this->getSubQuery() . ')';

        return $result;
    }

    /**
     * @return string
     */
    private function getSubQuery()
    {
        $result = 'SELECT ';
        $result .= $this->getSubQueryLines();
        $result .= ' FROM ' . static::TABLE_NAME;
        $result .= ' GROUP BY ' . static::USER_ID_FIELD;

        return $result;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getSubQueryLines()
    {
        $result = static::USER_ID_FIELD . ',';

        $countDates = count($this->dates);

        foreach ($this->dates as $key => $date) {
            $windowDateStart = new \DateTime($date);
            $windowDateEnd = new \DateTime($date);

            $windowDateStart->modify('-' . ($this->windowDays - 1) . ' day');
            $windowDateEnd->modify('-1 day');

            $line = self::SELECT_LINE_SUBQUERY_START . self::SELECT_LINE_SUBQUERY_PART1
                . self::SELECT_LINE_SUBQUERY_PART2 . self::SELECT_LINE_SUBQUERY_PART3
                . self::SELECT_LINE_SUBQUERY_END;
            $line = str_replace('{*date_field*}', static::DATE_FIELD, $line);
            $line = str_replace('{*retention_date*}', $date, $line);
            $line = str_replace('{*window_date_start*}', $windowDateStart->format('Y-m-d'), $line);
            $line = str_replace('{*window_date_end*}', $windowDateEnd->format('Y-m-d'), $line);
            $line = str_replace('{*key*}', ($key + 1), $line);

            $line .= $countDates == ($key + 1) ? '' : ',';
            $result .= ' ' . $line;
        }

        return $result;
    }

    /**
     * @return string
     */
    private function getSelectLines()
    {
        $result = '';
        $countDates = count($this->dates);
        foreach ($this->dates as $key => $date) {
            $line = str_replace('{*key*}', ($key + 1), self::SELECT_LINE);
            $line = str_replace('{*date*}', $this->dateToQueryString($date), $line);

            $result .= ' ' . $line;
            $result .= $countDates == ($key + 1) ? '' : ',';
        }

        return $result;
    }

    /**
     * @param string $date
     * @return string
     */
    private function dateToQueryString($date)
    {
        return str_replace('-', self::DATE_DELIMITER, $date);
    }

    /**
     * @param array $queryResult
     * @return array
     */
    private function resultQueryToArray(array $queryResult)
    {
        $result = [];
        $data = $queryResult[0];
        foreach ($this->dates as $date) {
            if (isset($data[$this->dateToQueryString($date)])) {
                $result[$date] = (integer) $data[$this->dateToQueryString($date)];
            }
        }

        return $result;
    }

    /**
     * @param IReportDateCache $cache
     */
    public function setCache(IReportDateCache $cache)
    {
        $this->cache = $cache;
    }

    protected function setDataFromCache()
    {
        $this->reportData = $this->cache->getData($this->startDate, $this->endDate, $this->windowDays);
    }
}
