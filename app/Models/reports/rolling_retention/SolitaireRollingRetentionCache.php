<?php

namespace App\Models\reports\rolling_retention;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolitaireRollingRetentionCache extends RollingRetentionCache
{
    protected $table = 'solitaire_rolling_retention_cache';
}
