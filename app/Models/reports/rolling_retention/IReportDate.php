<?php

namespace App\Models\reports\rolling_retention;

interface IReportDate
{
    public function getReportData();
    public function getWindowDays();
    public function getReportWithFooter();
    public function setCache(IReportDateCache $cache);
}
