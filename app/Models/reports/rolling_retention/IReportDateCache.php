<?php

namespace App\Models\reports\rolling_retention;

interface IReportDateCache
{
    public function getData($startDate, $endDate, $windowDays);
    public function setData(IReportDate $report, $dates = []);
}
