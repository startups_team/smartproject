<?php

namespace App\Console\Commands;

use App\Models\reports\rolling_retention\SolitaireRollingRetention;
use App\Models\reports\rolling_retention\SolitaireRollingRetentionCache;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class updateReportCache extends Command
{
    const DAYS = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:update-cache {--days=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update report cache --days=';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $days = $this->option('days');
        $endDate = (new \DateTime())->format('Y-m-d');
        $startDate = $endDate;

        if ($days) {
            $startDate = (new \DateTime())->modify('-' . $days . ' day')->format('Y-m-d');
        }

        $cache = new SolitaireRollingRetentionCache();

        $report = new SolitaireRollingRetention($startDate, $endDate);
        $report->startReport();
        $cache->setData($report);
    }
}
