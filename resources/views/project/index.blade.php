@extends('project.layout')

@section('content')
    <div class="row">
        <div class="col"><br></div>
    </div>
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            <form action="/" method="get">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="staticDates" class="sr-only">Диапазон дат</label>
                        <input type="text" readonly class="form-control-plaintext" id="staticDates" value="Диапазон дат">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="daterange" class="sr-only">Password</label>
                        <input type="text" class="form-control"
                               name="daterange" id="daterange" value="{{ $startDate }} - {{ $endDate }}" />
                    </div>
                    <div class="form-group col-md-3"></div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <div class="form-check">
                                <input name="cache" class="form-check-input"
                                       type="checkbox" id="gridCheckCache" <?php if ($useCache) { echo 'checked';} ?> value="1">
                                <label class="form-check-label" for="gridCheckCache">
                                    Подготовленные данные
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <button type="submit" class="btn btn-primary mb-2">Показать</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
        </div>
    </div>
    <div class="row">
        <div class="col"><br></div>
    </div>
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            Время выполнения скрипта: {{ $time }} cек
        </div>
        <div class="col-md-3">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            @include('project.table', ['data' => $data])
        </div>
        <div class="col-md-3">
        </div>
    </div>
@endsection
