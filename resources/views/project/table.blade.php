<?php $i = 0; ?>
<table class="table table-bordered">
    <tr>
        <th>№</th>
        <th>Дата</th>
        <th>Кол-во</th>
    </tr>
    @foreach ($data as $date => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $date }}</td>
            <td>{{ $value }}</td>
        </tr>
    @endforeach
</table>
